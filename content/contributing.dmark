---
title: "Contributing"
---

p. Nanoc is the effort of %ref[frag=contributors]{dozens of people}. Contributions are welcomed, no matter how small. This page shows the different ways you can make a difference to Nanoc.

h2. Making a donation

p. Nanoc is, and always will be, provided free of charge. Development is voluntary and happens entirely in free time. If you use Nanoc and like it, please consider showing your token of support by %ref[url=http://www.pledgie.com/campaigns/9282]{making a donation}.

h2. Reporting bugs

p. If you find a bug in Nanoc, you should %ref[url=https://github.com/nanoc/nanoc/issues/new]{report it}! But before you do, make sure you have the latest version of Nanoc (and dependencies) installed, and see if you can still reproduce the bug there. If you can, report it!

p. When reporting a bug, please make sure you include as many details as you can about the bug. Some information that you should include:

ul.
  li. The Nanoc version (%command{nanoc --version}), including the Ruby version
  li. The %filename{crash.log} file, if the bug you’re reporting is a crash
  li. Steps to reproduce the bug

h2. Submitting feature requests

p. If you have an idea for a new feature, start a discussion on the %ref[url=https://groups.google.com/forum/?fromgroups#!forum/nanoc]{Nanoc Google group}.

h2. Reviewing pull requests

p. We use pull requests extensively for Nanoc development. No pull request gets merged without at least one +1.

p. This approach ensures quality, but can be a bit slow. You can help out by checking %ref[url=https://github.com/pulls?q=is%%3Aopen+user%%3Ananoc+label%%3A%%22to+review%%22]{open pull requests marked as “to review”}.

p. If you think the pull request is good, leave a %code{:+1:}. If you have concerns, leave a comment.

h2. Contributing code

p. To contribute code, you need a basic knowledge of git. The %ref[url=http://try.github.io/]{Try Git} interactive tutorial is quite good for getting up to speed.

p. Before you start coding, make sure that the idea you have fits with Nanoc’s philosophy. Start a thread on the %ref[url=http://groups.google.com/group/nanoc]{discussion group} or find us on the %ref[url=irc://chat.freenode.net/#nanoc]{IRC channel}. Generally speaking, all bug fixes are accepted, while feature changes need more discussion.

note. For all changes, backwards compatibility %emph{must} be retained. This means that you can add a feature, but you cannot modify a feature to work in a different way.

p. To fetch the latest Nanoc source code, clone the Git repository:

listing.
  %prompt{~%%} %kbd{git clone git://github.com/nanoc/nanoc.git}

p. Pick the right branch to start off:

ul.
  li. If you want to add a feature, use the %code{master} branch.
  li. If you want to add a bug fix, use the %code{release-4.1.x} branch.

p. You can switch to the right branch using %command{git checkout}:

listing.
  %prompt{nanoc%%} %kbd{git checkout release-4.1.x}

p. Create a branch off one of the two existing branches mentioned above. Pick a good name; the convention is to prefix the branch name with %code{bug/} when it is a bug and with %code{feature/} if it is a feature. Once you’ve picked a branch name, create the branch:

listing.
  %prompt{nanoc%%} %kbd{git checkout -b bug/fix-colors-on-windows}

p. Nanoc uses %ref[url=http://bundler.io/]{Bundler} to manage its development dependencies. Run %kbd{bundle install} to install all dependencies necessary for development and testing.

p. Now you can make modifications to the source code. You can find the source code in %filename{lib} and the tests in %filename{test}. Make sure that your changes have test cases that cover the bug fix or the new/changed functionality. To run the tests, run %kbd{bundle exec rake}.

p. To test your locally modified version of Nanoc on a local Nanoc site, edit your site’s %filename{Gemfile} and let the Nanoc gem point to the locally modified version:

listing[lang=ruby].
  gem 'nanoc', path: '/home/denis/projects/nanoc'

note. You can also invoke Nanoc by calling %command{nanoc} using the full path to the %filename{bin/nanoc} in your cloned repository. However, we recommend using %ref[url=http://bundler.io/]{Bundler} instead.

p. After making your modifications, make sure that the source code documentation is up-to-date. Nanoc uses %ref[url=http://yardoc.org/]{YARD} for its source code docs. The %ref[url=http://rubydoc.info/gems/yard/file/doc/GettingStarted.md]{YARD getting started guide} is a helpful resource when writing YARD documentation.

p. Finally, create a pull request. Make sure the submit your pull request against the branch you originally started off (%code{master} or %code{release-4.1.x}).

p. Once submitted, your work here is done. We’ll review the code, have a discussion and merge it once we’re satisfied.

h2. Releasing Nanoc

p. If you’re a release manager, you can follow these steps to release a new version of Nanoc. Before you start, make sure that you’ve got the approval of at least one other release manager (and preferably also the approval of Denis).

caution. Being a release manager grants you considerable power, but remember that with great power comes great responsibility.

h3. Requirements

p. Before you start, ensure that you have access to the following:

ul.
  li. GitHub push access
  li. RubyGems push access
  li. IRC channel operator access
  li. Web site push access

p. If you are missing any of these, let me (Denis) know and I’ll set you up.

h3. Preparing for a release

p. Preparing a release means ensuring that the version that is about to be released meets the requirements. To prepare for a release, follow these steps:

ol[spacious].
  li. Ensure the %code{Nanoc::VERSION} constant is set to the right version. Keep in mind that Nanoc follows the %ref[url=http://semver.org/]{Semantic Versioning} standard.

  li. Ensure the release notes in the %filename{NEWS.md} file are up-to-date, and that the release date is correct.

  li. Run the tests using %kbd{bundle exec rake}.

  li. As a final check, compile the Nanoc site with the Nanoc gem in the %filename{Gemfile} pointing to your local Nanoc working copy, and verify locally that the release notes page is as expected.

h3. Releasing the new version

p. Once the preparation is complete, the new version can be released. To release a new version of Nanoc, follow these steps:

ol[spacious].
  li. Build the gem (%kbd{gem build nanoc.gemspec}).

  li. Tag the release using %kbd{git tag --sign --annotate %var{1.2.3} --message 'Version %var{1.2.3}'}, replacing %var{1.2.3} with the new version number. Signing Git tags is optional, but highly recommended.

  li. Push the gem using %kbd{gem push nanoc-*.gem}.

  li. Push the changes to GitHub (%kbd{git push}). Don’t forget to also push the tags (%kbd{git push --tags}).

h3. Spread the word

p. To announce the new release, follow these steps:

ol[spacious].
  li. Update the release notes on site. This only involves recompiling the site with the new version of Nanoc (the release notes on the site are extracted from the %filename{NEWS.md} file in the Nanoc gem) and pushing the site (%kbd{bundle exec nanoc deploy}).

  li. Update the release notes on GitHub. Create a new release for the tag, set the release title to the new version number, and copy-paste the release notes into the description field.

  li. Send an announcement e-mail to the %ref[url=http://groups.google.com/group/nanoc]{Nanoc mailing list}. Include the version number, link to the release notes, instructions for how to update (using plain RubyGems and using Bundler), and instructions on how to report issues. The e-mail template that I often use is based off the following:

  listing[template].
    Hi,

    Nanoc %var{version} is out. This %var{major/minor/patch} release %var{fixes a bug related to X/adds enhancements X and Y/adds feature X}. You can find the full release notes at the bottom of this e-mail or at http://nanoc.ws/release-notes/.

    You can update your gems using `gem update`. If you use Bundler (which I recommend), run `bundle update` to get the latest version of Nanoc.

    Do report any bugs you find on the GitHub issue tracker at https://github.com/nanoc/nanoc/issues/new.

    Cheers,

    Denis

    %var{RELEASE NOTES}

  li. Update the topic of the %uri{#nanoc} IRC channel.

  li. Update the version mentioned on the %ref[url=https://en.wikipedia.org/wiki/Nanoc]{Nanoc Wikipedia page}.

h2. Contributor Code of Conduct

p. As contributors and maintainers of this project, we pledge to respect all people who contribute through reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, and other activities.

p. We are committed to making participation in this project a harassment-free experience for everyone, regardless of level of experience, gender, gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, or religion.

p. Examples of unacceptable behavior by participants include the use of sexual language or imagery, derogatory comments or personal attacks, trolling, public or private harassment, insults, or other unprofessional conduct.

p. Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct. Project maintainers who do not follow the Code of Conduct may be removed from the project team.

p. This code of conduct applies both within project spaces and in public spaces when an individual is representing the project or its community.

p. Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by opening an issue or contacting one or more of the project maintainers.

p. This Code of Conduct is adapted from the %ref[url=http://contributor-covenant.org]{Contributor Covenant}, version 1.1.0, available %ref[url=http://contributor-covenant.org/version/1/1/0/]{here}.

h2. Contributors

p. These people have contributed to Nanoc already:

p. %emph{%erb{@items['/contributing/_contributors'].raw_content}}
